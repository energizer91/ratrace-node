var Menu = require('../helpers/menu');
var chai = require('chai');

var expect = chai.expect;

var menu = new Menu({
    test: {
        item: {
            type: 'MenuItem'
        },
        handler: function (params, methods, helpers) {
            return params[0] + ' ' + helpers.foo;
        }
    }
});

describe('Menu', function () {
    it('should create properly', function () {
        expect(menu).to.be.a('object');
    });

    describe('Error handling', function () {
        it('should throw an error on empty path', function () {
            expect(menu.getMenu.bind(menu, '')).to.throw('Menu path is empty');
        });

        it('should throw an error on invalid path', function () {
            expect(menu.getMenu.bind(menu, 'lol.arbidol')).to.throw('Unknown menu');
        });
    });

    describe('Simple menu test.item', function () {
        it('should return object', function () {
            expect(menu.getMenuItem(['test', 'item'])).to.have.property('type').to.equal('MenuItem');
        });

        it('should ignore helpers etc.', function () {
            expect(menu.getMenu('test.item 123', {foo: 'bar'})).to.have.property('type').to.equal('MenuItem');
        });
    });

    describe('Complicated menu test.handler', function () {
        it('should return function', function () {
            expect(menu.getMenuItem(['test', 'handler'])).to.be.a('function');
        });

        it('should pass params on test.handler', function () {
            expect(menu.getMenu('test.handler 123', {foo: 'bar'})).to.equal('123 bar');
        });
    });
});