/**
 * Created by Александр on 13.12.2015.
 */
module.exports = function (express, botApi, configs) {
    var router = express.Router();
    var botMenu = new botApi.menu(
        {
            common: {
                start: {
                    type: 'menuItem',
                    buttons: [
                        [
                            {
                                text: 'Мои игры',
                                callback_data: 'game.list'
                            }
                        ],
                        [
                            {
                                text: 'Игры со мной',
                                callback_data: 'game.joined'
                            }
                        ]
                    ],
                    text: 'Добро пожаловать!'
                }
            },
            game: {
                create: function (params, methods, helpers) {
                    return methods.createGame(helpers.user).then(function (game) {
                        return {
                            text: 'Игра создана!',
                            buttons: methods.getGameInfoButtons(game._id)
                        }
                    })
                },
                list: function (params, methods, helpers) {
                    var skipStep = 5,
                        skipParam = parseInt(params[0] || 0, 10);
                    return methods.getGames({author: helpers.user}).count().then(function (count) {
                        return methods.getGames({author: helpers.user}).skip(skipParam).limit(skipStep).then(function (games) {
                            var gamesMenu = '',
                                gamesNames = [];

                            if (skipParam) {
                                gamesNames.push({
                                    text: '<<',
                                    callback_data: 'game.list ' + (skipParam - skipStep)
                                })
                            }

                            games.forEach(function (game, index) {
                                gamesMenu += '\n' + (skipParam + index + 1) + ': ' + game.name || 'Игра без названия';
                                gamesNames.push({
                                    text: (skipParam + index + 1),
                                    callback_data: 'game.join ' + game._id
                                });
                            });

                            if (skipParam + skipStep < count) {
                                gamesNames.push({
                                    text: '>>',
                                    callback_data: 'game.list ' + (skipParam + skipStep)
                                })
                            }

                            return {
                                text: games.length ? 'Выберите игру из списка ниже:' + gamesMenu : 'У вас нет еще игр',
                                buttons: [gamesNames].concat([
                                    [
                                        {
                                            text: 'Создать игру',
                                            callback_data: 'game.create'
                                        },
                                        {
                                            text: 'Назад',
                                            callback_data: 'common.start'
                                        }
                                    ]
                                ])
                            }
                        });
                    });
                },
                join: function (params, methods, helpers) {
                    return methods.getGame({_id: params[0]}).then(function (game) {
                        var gameTitle = 'Игра: ' + game.name + '\n' +
                            'Призовой фонд: ' + game.prize + '\n' +
                            'Количество точек: ' + game.locations.length + '\n' +
                            'Команд: ' + game.teams.length + '\n' +
                            'Сложность: хз, еще не научился считать, но там типа надо будет по расстоянию между точками';

                        var buttons = [
                            [
                                {
                                    text: 'Участвовать',
                                    callback_data: 'game.info ' + game._id
                                },
                                {
                                    text: 'Смотреть',
                                    callback_data: 'game.spectate ' + game._id
                                }
                            ]
                        ];

                        if (game.user = helpers.user) {
                            buttons.push([
                                {
                                    text: 'Редактировать',
                                    callback_data: 'game.edit ' + game._id
                                }
                            ]);
                        }

                        buttons.push([
                            {
                                text: 'Назад',
                                callback_data: 'game.list'
                            }
                        ]);

                        return {
                            text: gameTitle,
                            buttons: buttons
                        }
                    });
                },
                info: function (params, methods) {
                    return methods.getGame({_id: params[0]}).then(function (game) {
                        var teams = methods.getTeamsList(game.teams, game._id, params[1], 'game.info', 'team.info'),
                            availableTeams = teams.teamButtons,
                            availableTeamNames = teams.teamNames;

                        return {
                            text: 'Выберите команду для вступления:\n(Капитанов/Штурманов/Зрителей)\n\n' + availableTeamNames.join('\n'),
                            buttons: [
                                availableTeams,
                                [
                                    {
                                        text: 'Создать свою',
                                        callback_data: 'team.create ' + game._id
                                    },
                                    {
                                        text: 'Вернуться назад',
                                        callback_data: 'game.join ' + game._id
                                    }
                                ]
                            ]
                        };
                    })
                },
                spectate: function (params, methods) {
                    return methods.getGame({_id: params[0]}).then(function (game) {
                        var teams = methods.getTeamsList(game.teams, game._id, params[1], 'game.spectate', 'team.spectate'),
                            availableTeams = teams.teamButtons,
                            availableTeamNames = teams.teamNames;

                        return {
                            text: 'Выберите команду для слежения:\n(Капитанов/Штурманов/Зрителей)\n\n' + availableTeamNames.join('\n'),
                            buttons: [
                                availableTeams,
                                [
                                    {
                                        text: 'Вернуться назад',
                                        callback_data: 'game.join ' + game._id
                                    }
                                ]
                            ]
                        };
                    })
                },
                edit: function (params, methods) {
                    return {
                        text: 'Отредактируйте свою игру',
                        buttons: methods.getGameInfoButtons(params[0])
                    };
                },
                name: {
                    type: 'menuAction',
                    text: 'Введите сюда название игры'
                },
                prize: {
                    type: 'menuAction',
                    text: 'Введите приз'
                },
                locations: {
                    type: 'menuAction',
                    text: 'Присылайте сюда геометки одна за другой. Как закончите - пришлите мне /done'
                },
                delete: {
                    type: 'menuAction',
                    text: 'Если действительно хотите удалить игру, то напишите мне текстом "удалить"',
                    buttons: [
                        [
                            {
                                text: 'Назад',
                                callback_data: 'game.list'
                            }
                        ]
                    ]
                },
                publish: function (params, methods, helpers) {
                    return methods.getGame({_id: params[0], author: helpers.user}).then(function (game) {
                        if (!game) {
                            throw new Error('Ошибка доступа: Вы не являетесь автором этой игры.');
                        }

                        game.status = 'ready';
                        return game.save();
                    }).then(function (savedGame) {
                        var publishUrl = 'https://t.me/' + configs.bot.botName + '?start=' + savedGame._id;

                        return {
                            text: 'Игра опубликована. Поделитесь с друзьями этой ссылкой: ' + publishUrl
                        };
                    });
                },
                start: function (params, methods, helpers) {
                    return methods.getGame({_id: params[0], author: helpers.user}).populate({path: 'teams.users.user', select: 'user_id', model: botApi.mongo.User}).then(function (game) {
                        if (!game) {
                            throw new Error('Ошибка доступа: Вы не являетесь автором этой игры.');
                        }

                        if (!game.teams.length) {
                            throw new Error('Не создано ни одной команды.');
                        }

                        if (!game.locations.length) {
                            throw new Error('Не задано ни одной метки для игры.');
                        }

                        if (!game.prize) {
                            throw new Error('Не задан призовой фонд.');
                        }

                        var allTeamsReady = game.teams.reduce(function (notFinished, team) {
                            return notFinished && team.status === 'ready';
                        }, true);

                        if (!allTeamsReady) {
                            throw new Error('Не все команды готовы.');
                        }

                        // if (game.status !== 'ready') {
                        //     throw new Error('Игра не готова к запуску или уже была завершена.');
                        // }

                        game.status = 'started';

                        return game;
                    }).then(function (game) {
                        var teams = game.teams,
                            teamsWithoutUpdateId = [],
                            teamMembers = teams.reduce(function (list, team) {
                                return list.concat(team.users);
                            }, []),
                            broadcastList = teamMembers.map(function (user) {
                                return user.user.user_id
                            });

                        for (var i = 0; i < teams.length; i++) {
                            if (!teams[i].updateId) {
                                teamsWithoutUpdateId.push(teams[i].name || ('Команда без названия ' + (i + 1)));
                                break;
                            }

                            teams[i].progress.push({
                                location: game.locations[0]
                            });
                        }

                        if (teamsWithoutUpdateId.length) {
                            throw new Error('Невозможно запустить игру: следующие команды не сообщили лайвлокейшн:\n' + teamsWithoutUpdateId.join(', '))
                        }

                        return game.save().then(function (savedGame) {
                            return methods.broadcastLocation(broadcastList, savedGame.locations[0]);
                        });
                    }).then(function () {
                        return {
                            text: 'Игра Началась. Метки разосланы.'
                        };
                    });
                }
            },
            team: {
                create: function (params, methods, helpers) {
                    return methods.createTeam(params[0], helpers.user).then(function (data) {
                        return {
                            text: 'Команда создана!',
                            buttons: methods.getTeamInfoButtons(data.team, data.game._id, true)
                        }
                    });
                },
                name: {
                    type: 'menuAction',
                    text: 'Пришлите сюда название команды'
                },
                link: function (params, methods) {
                    return methods.getTeamById(params[0]).then(function (team) {
                        if (team.updateId) {
                            throw new Error('Геопозиция уже привязана. Сбросьте предыдущую.');
                        }

                        return {
                            type: 'menuAction',
                            text: 'Пришлите сюда live location с максимальным интервалом.'
                        }
                    })
                },
                unlink: function (params, methods) {
                    return methods.getGame({'teams._id': params[0]}).then(function (game) {
                        if (!game) {
                            throw new Error('Игра не найдена');
                        }

                        var team = game.teams.id(params[0]);

                        if (!team) {
                            throw new Error('Команда не найдена')
                        }

                        team.updateId = 0;

                        return game.save();
                    }).then(function () {
                        return {
                            text: 'Геометка успешно удалена.',
                            buttons: [
                                [
                                    {
                                        text: 'Назад',
                                        callback_data: 'team.info ' + params[0]
                                    }
                                ]
                            ]
                        }
                    })
                },
                info: function (params, methods, helpers) {
                    return methods.getTeamById(params[0]).then(function (team) {
                        return {
                            text: methods.getTeamInfo(team),
                            buttons: methods.getTeamInfoButtons(team, team.ownerDocument()._id, team.users.filter(function (user) {
                                return user.user.toString() === helpers.user._id.toString() && user.role === 'owner';
                            }).length)
                        }
                    })
                },
                spectate: function (params, methods, helpers) {
                    return methods.getGame({'teams._id': params[0]}).populate({path: 'author', select: 'user_id', model: botApi.mongo.User}).then(function (game) {
                        if (!game) {
                            throw new Error('Игра не найдена');
                        }

                        var team = game.teams.id(params[0]);

                        if (!team) {
                            throw new Error('Команда не найдена')
                        }

                        var userIsForeign = game.teams.findIndex(function (t) {
                            return (t.users.findIndex(function (user) {
                                return user.user.toString() === helpers.user._id.toString();
                            }) >= 0) && t !== team;
                        }) && game.author._id.toString() !== helpers.user._id.toString();

                        if (userIsForeign) {
                            throw new Error('Просматривать чужие команды запрещено');
                        }

                        if (team.spectators.findIndex(function (spectator) {
                                return spectator.user.toString() === helpers.user._id.toString();
                            }) >= 0) {
                            throw new Error('Вы уже следите за этой командой');
                        }

                        if (!team.updateId) {
                            throw new Error('У команды еще не привязана геопозиция');
                        }

                        if (!team.location.length) {
                            throw new Error('У команды еще нет геометок');
                        }

                        var lastLocation = team.location[team.location.length - 1];

                        return botApi.bot.sendLocation(helpers.user.user_id, lastLocation.location, {live: true}).then(function (response) {
                            team.spectators.push({
                                user: helpers.user,
                                updateId: response.result.message_id
                            });

                            return game.save();
                        }).then(function () {
                            return team;
                        });
                    }).then(function (savedTeam) {
                        return {
                            text: 'Начинается трансляция команды ' + savedTeam.name,
                            buttons: [
                                [
                                    {
                                        text: 'Назад',
                                        callback_data: 'team.info ' + params[0]
                                    }
                                ]
                            ]
                        }
                    })
                },
                unspectate: function (params, methods, helpers) {
                    return methods.getGame({'teams._id': params[0]}).then(function (game) {
                        if (!game) {
                            throw new Error('Игра не найдена');
                        }

                        var team = game.teams.id(params[0]);

                        if (!team) {
                            throw new Error('Команда не найдена')
                        }

                        var spectateIndex = team.spectators.findIndex(function (spectator) {
                            return spectator.user.toString() === helpers.user._id.toString();
                        });

                        if (spectateIndex < 0) {
                            throw new Error('Вы не найдены в списке зрителей');
                        }

                        var deletedTeam = team.spectators.splice(spectateIndex, 1);

                        // game.teams[teamIndex].updateId = 0;

                        return botApi.bot.stopLiveLocation(helpers.user.user_id, deletedTeam[0].updateId).catch(function (error) {
                            return botApi.bot.sendMessage(helpers.user.user_id, error.message);
                        }).then(function () {
                            return game.save();
                        }).then(function () {
                            return team;
                        });
                    }).then(function (savedTeam) {
                        return {
                            text: 'Вы перестали следить за командой ' + savedTeam.name,
                            buttons: [
                                [
                                    {
                                        text: 'Назад',
                                        callback_data: 'team.info ' + params[0]
                                    }
                                ]
                            ]
                        }
                    })
                }
            }
        },
        {
            createGame: function (author) {
                var newGame = new botApi.mongo.Game({
                    author: author,
                    status: 'new'
                });

                return newGame.save();
            },
            getGame: function (condition) {
                return botApi.mongo.Game.findOne(condition);
            },
            getGames: function (condition) {
                return botApi.mongo.Game.find(condition);
            },
            getTeamsList: function (teams, gameId, skip, menuCallback, teamCallback) {
                var skipParam = parseInt(skip || 0, 10);
                var skipStep = 5;
                var availableTeams = [],
                    avaliableTeamNames = [];

                if (skipParam) {
                    availableTeams.push({
                        text: '<<',
                        callback_data: menuCallback + ' ' + gameId + (skipParam - skipStep)
                    })
                }

                teams.slice(skipParam, skipParam + skipStep).forEach(function (team, index) {
                    availableTeams.push({
                        text: index + 1,
                        callback_data: teamCallback + ' ' + team._id
                    });
                    avaliableTeamNames.push((index + 1) + ': ' + this.getTeamInfo(team));
                }, this);

                if (skipParam + skipStep < teams.length) {
                    availableTeams.push({
                        text: '>>',
                        callback_data: menuCallback + ' ' + gameId + (skipParam + skipStep)
                    })
                }

                return {
                    teamNames: avaliableTeamNames,
                    teamButtons: availableTeams
                };
            },
            getGameInfoButtons: function (gameId) {
                return [
                    [
                        {
                            text: 'Задать название',
                            callback_data: 'game.name ' + gameId
                        },
                        {
                            text: 'Задать точки',
                            callback_data: 'game.locations ' + gameId
                        }
                    ],
                    [
                        {
                            text: 'Задать приз',
                            callback_data: 'game.prize ' + gameId
                        },
                        {
                            text: 'Опубликовать игру',
                            callback_data: 'game.publish ' + gameId
                        }
                    ],
                    [
                        {
                            text: 'Начать игру',
                            callback_data: 'game.start ' + gameId
                        }
                    ],
                    [
                        {
                            text: 'Удалить игру',
                            callback_data: 'game.delete ' + gameId
                        }
                    ],
                    [
                        {
                            text: 'Назад',
                            callback_data: 'game.join ' + gameId
                        }
                    ]
                ];
            },
            createTeam: function (gameId, author) {
                return botApi.mongo.Game.findById(gameId).then(function (game) {
                    var teamIndex = findTeamByUser(game.teams, author._id.toString());

                    if (teamIndex >= 0) {
                        throw new Error('Вы уже состоите в команде');
                    }

                    game.teams.push({
                        users: [
                            {
                                user: author,
                                role: 'owner'
                            }
                        ]
                    });

                    return game.save().then((function (savedGame) {
                        var teamIndex = findTeamByUser(savedGame.teams, author._id.toString());

                        return {
                            team: savedGame.teams[teamIndex],
                            game: savedGame
                        };
                    }).bind(this));
                });
            },
            getTeamInfoButtons: function (team, gameId, isOwner) {
                var buttons = [];
                if (isOwner) {
                    var firstRow = [{
                        text: 'Задать название',
                        callback_data: 'team.name ' + team._id
                    }];
                    if (team.updateId) {
                        firstRow.push({
                            text: 'Отвязать геопозицию',
                            callback_data: 'team.unlink ' + team._id
                        })
                    } else {
                        firstRow.push({
                            text: 'Привязать геопозицию',
                            callback_data: 'team.link ' + team._id
                        })
                    }
                    buttons.push(firstRow);
                }

                if (isOwner) {
                    buttons.push([
                        {
                            text: 'Вступить',
                            callback_data: 'team.join ' + team._id
                        },
                        {
                            text: 'Удалить команду',
                            callback_data: 'team.delete ' + team._id
                        }
                    ]);
                } else {
                    buttons.push([
                        {
                            text: 'Вступить',
                            callback_data: 'team.join ' + team._id
                        }
                    ])
                }

                buttons.push([
                    {
                        text: 'Следить',
                        callback_data: 'team.spectate ' + team._id
                    },
                    {
                        text: 'Не следить',
                        callback_data: 'team.unspectate ' + team._id
                    }
                ]);

                buttons.push([
                    {
                        text: 'Назад',
                        callback_data: 'game.info ' + gameId
                    }
                ]);

                return buttons;
            },
            updateGame: function (gameId, data) {
                return botApi.mongo.Game.findOneAndUpdate({_id: gameId}, data);
            },
            findTeamByUser: function (teams, userId) {
                return teams.findIndex(function (team) {
                    return team.users.findIndex(function (user) {
                        return user.user.toString() === userId;
                    }) >= 0;
                });
            },
            getTeamInfo: function (team) {
                if (!team) {
                    return 'Команда не найдена';
                }
                return 'Команда ' + (team.name || 'без названия') + ' (' + team.users.reduce(function (counter, user) {
                    if (user.role === 'owner') {
                        counter[0] += 1;
                    } else if (user.role === 'navigator') {
                        counter[1] += 1;
                    } else {
                        counter[2] += 1;
                    }

                    return counter;
                }, [0, 0, 0]).join('/') + ')'
            },
            getTeamById: function (teamId) {
                return botApi.mongo.Game.findOne({"teams._id": teamId}).then((function (game) {
                    if (!game) {
                        throw new Error('Игра не найдена');
                    }

                    var team = game.teams.id(teamId);

                    if (!team) {
                        throw new Error('Команда не найдена')
                    }

                    return team;
                }).bind(this));
            },
            getTeamByUpdateId: function (updateId) {
                return botApi.mongo.Game.findOne({"teams.updateId": updateId}).then(function (game) {
                    var teamIndex = findTeamByUpdateId(game.teams, updateId);

                    if (teamIndex < 0) {
                        throw new Error('Команда не найдена')
                    }

                    return game.teams[teamIndex];
                });
            },
            broadcastLocation: function (userIds, location) {
                return botApi.bot.sendLocations(userIds, location)
            }
        }
    );

    var generateUserInfo = function (user) {
            return {
                text: '```\n' +
                'User ' + user.user_id + ':\n' +
                'Имя:        ' + (user.first_name    || 'Не указано') + '\n' +
                'Фамилия:    ' + (user.last_name     || 'Не указано') + '\n' +
                'Ник:        ' + (user.username      || 'Не указано') + '\n' +
                'Подписка:   ' + (user.subscribed     ? 'Подписан' : 'Не подписан') + '\n' +
                'Фидбэк:     ' + (user.feedback_mode  ? 'Включен'  : 'Выключен') + '\n' +
                'Админ:      ' + (user.admin          ? 'Присвоен' : 'Не присвоен') + '\n' +
                'Бан:        ' + (user.banned         ? 'Забанен'  : 'Не забанен') + '\n' +
                'Язык:       ' + (user.language      || 'Не выбран') + '\n' +
                'Клавиатура: ' + (user.keyboard       ? 'Включена' : 'Выключена') + '\n' +
                'Платформа:  ' + (user.client        || 'Не выбрана') + '```'
            };
        },
        generateStatistics = function (interval, stats) {
            return {
                text: '```\n' +
                'Статистика за ' + interval + ':\n' +
                'Пользователи\n' +
                'Всего:                  ' + stats.users.count + '\n' +
                'Новых:                  ' + stats.users.new + '\n' +
                'Удаленных:              ' + stats.users.deleted + '\n' +
                'Сообщения\n' +
                'Всего:                  ' + stats.messages.received + '```'
            };
        },
        getDistance = function (location1, location2) {
            var radlat1 = Math.PI * location1.latitude / 180;
            var radlat2 = Math.PI * location2.latitude / 180;
            var theta = location1.longitude - location2.longitude;
            var radtheta = Math.PI * theta/180;

            var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);

            dist = Math.acos(dist);
            dist = dist * 180/Math.PI;
            dist = dist * 60 * 1.1515;

            return dist * 1.609344 * 1000;
        },
        findTeam = function (teams, teamId) {
            return teams.findIndex(function (team) {
                return team._id.toString() === teamId;
            });
        },
        sendGameInfoMessage = function (chatId, savedGame, message) {
        var params = {
            buttons: [
                [
                    {
                        text: 'Задать название',
                        callback_data: 'game.name ' + savedGame._id
                    },
                    {
                        text: 'Задать точки',
                        callback_data: 'game.locations ' + savedGame._id
                    }
                ],
                [
                    {
                        text: 'Задать приз',
                        callback_data: 'game.prize ' + savedGame._id
                    },
                    {
                        text: 'Опубликовать игру',
                        callback_data: 'game.publish ' + savedGame._id
                    }
                ],
                [
                    {
                        text: 'Начать игру',
                        callback_data: 'game.start ' + savedGame._id
                    }
                ],
                [
                    {
                        text: 'Удалить игру',
                        callback_data: 'game.delete ' + savedGame._id
                    }
                ],
                [
                    {
                        text: 'Назад',
                        callback_data: 'game.join ' + savedGame._id
                    }
                ]
            ]

        };

        if (message.text) {
            return botApi.bot.editMessage(chatId, message, params);
        }

            return botApi.bot.sendMessage(chatId, {text: message}, params);
        },
        sendTeamInfoMessage = function (chatId, savedTeam, gameId, message) {
            var params = {
                buttons: [

                    [
                        {
                            text: 'Задать название',
                            callback_data: 'team.name ' + savedTeam._id
                        },
                        {
                            text: 'Привязать геопозицию',
                            callback_data: 'team.link ' + savedTeam._id
                        }
                    ],
                    [
                        {
                            text: 'Информация',
                            callback_data: 'team.status ' + savedTeam._id
                        },
                        {
                            text: 'Удалить команду',
                            callback_data: 'team.delete ' + savedTeam._id
                        }
                    ],
                    [
                        {
                            text: 'Назад',
                            callback_data: 'game.info ' + gameId
                        }
                    ]
                ]
            };

            if (message.text) {
                return botApi.bot.editMessage(chatId, message, params);
            }

            return botApi.bot.sendMessage(chatId, {text: message}, params);
        },
        createGame = function (chatId, user, message) {
            var newGame = new botApi.mongo.Game({
                author: user,
                status: 'new'
            });

            return newGame.save().then(function (savedGame) {
                return sendGameInfoMessage(chatId, savedGame, message);
            })
        },
        sendMenuMessage = function (chatId, message, buttons) {
            if (message && message.text) {
                return botApi.bot.editMessage(chatId, message, {
                    buttons: buttons
                });
            }

            return botApi.bot.sendMessage(chatId, {text: message}, {
                buttons: buttons
            });
        },
        commands = {
            '/create_game': function (command, message, user) {
                return createGame(message.chat.id, user, 'Отлично! игра создана.');
            },
            '/done': function (command, message, user) {
                return botApi.mongo.User.findOneAndUpdate({user_id: user.user_id}, {pendingAnswer: ''}).then(function () {
                    return botMenu.getMenu('game.list', {
                        user: user
                    });
                }).then(function (result) {
                    return botApi.bot.sendMessage(message.chat.id, {text: result.text}, {
                        params: {
                            buttons: result.buttons
                        }
                    });
                });
            },
            '/broadcast': function (command, message, user) {
                if (!user.admin) {
                    throw new Error('Unauthorized access');
                }

                if (command.length <= 1) {
                    return botApi.bot.sendMessage(message.chat.id, botApi.dict.translate(user.language, 'broadcast_text_missing'));
                }

                command.splice(0, 1);

                return botApi.mongo.User.find({subscribed: true/*user_id: {$in: [85231140, 5630968, 226612010]}*/}).then(function (users) {
                    return botApi.request.fulfillAll(users.map(function (user) {
                        return this.sendMessage(user.user_id, command.join(' '));
                    }, botApi.bot));
                }).then(botApi.bot.sendMessage.bind(botApi.bot, message.chat.id, 'Рассылка окончена.'));

            },
            '/grant': function (command, message, user) {
                if (!user.admin) {
                    throw new Error('Unauthorized access');
                }

                if (command.length <= 1) {
                    return botApi.bot.sendMessage(message.chat.id, 'Введите id пользователя.');
                }

                var privileges = {};

                if (command[2]) {
                    if (command[2] === 'admin') {
                        privileges.admin = true;
                    } else if (command[2] === 'editor') {
                        privileges.editor = true;
                    }
                } else {
                    privileges.admin = false;
                    privileges.editor = false;
                }

                return botApi.mongo.User.findOneAndUpdate({user_id: parseInt(command[1])}, privileges).then(function () {
                    return botApi.bot.sendMessage(parseInt(command[1]), 'Вам были выданы привилегии администратора пользователем ' + user.first_name + '(' + user.username + ')');
                }).then(botApi.bot.sendMessage.bind(botApi.bot, message.chat.id, 'Привилегии присвоены.'));
            },
            '/user': function (command, message, user) {
                if (command[1] === 'count') {
                    return botApi.mongo.User.count().then(function (count) {
                        return botApi.bot.sendMessage(message.chat.id, botApi.dict.translate(user.language, 'current_user_count', {count: count}));
                    })
                } else if (command[1] === 'subscribed') {
                    return botApi.mongo.User.find({subscribed: true}).count().then(function (count) {
                        return botApi.bot.sendMessage(message.chat.id, botApi.dict.translate(user.language, 'current_subscribed_user_count', {count: count}));
                    })
                } else if (command[1] === 'id') {
                    if (command[2]) {
                        return botApi.mongo.User.findOne({user_id: command[2]}).then(function (user) {
                            return botApi.bot.sendMessage(message.chat.id, generateUserInfo(user), {disableButtons: true, parse_mode: 'Markdown'});
                        })
                    }
                    return botApi.bot.sendMessage(message.chat.id, botApi.dict.translate(user.language, 'current_user_id', {user_id: message.from.id}));
                }
                return botApi.bot.sendMessage(message.chat.id, generateUserInfo(user), {disableButtons: true, parse_mode: 'Markdown'});
            },
            '/find_user': function (command, message) {
                return botApi.mongo.User.findOne({username: command[1]}).then(function (user) {
                    return botApi.bot.sendMessage(message.chat.id, generateUserInfo(user), {disableButtons: true, parse_mode: 'Markdown'});
                })
            },
            '/start': function (command, message, user) {
                if (command[1] && command[1] === 'donate') {
                    return botApi.bot.sendInvoice(message.from.id, {
                        title: 'Донат на развитие бота',
                        description: 'А то совсем нечего кушать',
                        payload: command[1]
                    })
                } else if (command[1]) {
                    return botMenu.getMenu('game.join ' + command[1], {
                        user: user
                    }).then(function (result) {
                        return sendMenuMessage(message.from.id, result.text, result.buttons);
                    })
                }

                var result = botMenu.getMenu('common.start', {
                    user: user
                });

                return sendMenuMessage(message.from.id, result.text, result.buttons);
            },
            '/help': function (command, message, user) {
                return botApi.bot.sendMessage(message.chat.id, botApi.dict.translate(user.language, 'start'));
            },
            '/chat': function (command, message) {
                if (!configs.bot.baneksLink) {
                    return botApi.bot.sendMessage(message.chat.id, 'денис дурак');
                }
                return botApi.bot.sendMessage(message.chat.id, 'Здесь весело: ' + configs.bot.baneksLink);
            },
            '/stat': function (command, message) {
                var startDate,
                    startTitle = 'всё время',
                    now = new Date();
                if (command[1]) {
                    if (command[1] === 'day') {
                        startDate = new Date(now.getFullYear(), now.getMonth(), now.getDate());
                        startTitle = 'день';
                    } else if (command[1] === 'month') {
                        startDate = new Date(now.getFullYear(), now.getMonth());
                        startTitle = 'месяц';
                    } else if (command[1] === 'week') {
                        startDate = new Date(now.getFullYear(), now.getMonth(), now.getDate() - (now.getDay() || 7) + 1);
                        startTitle = 'неделю';
                    } else {
                        startDate = new Date(now.getFullYear());
                    }
                } else {
                    startDate = new Date(now.getFullYear());
                }

                return botApi.statistics.getOverallStatistics(
                    botApi.mongo,
                    startDate,
                    new Date()
                ).then(function (results) {
                    return botApi.bot.sendMessage(message.chat.id, generateStatistics(startTitle, results), {disableButtons: true, parse_mode: 'Markdown'});
                })
            },
            '/add': function (command, message, user) {
                if (!(user.admin || user.editor)) {
                    throw new Error('Access denied.');
                }
                if (message && message.chat && message.from && (message.chat.id !== message.from.id)) {
                    return botApi.bot.sendMessage(message.chat.id, 'Комменты недоступны в группах.');
                }
                if (user.add_mode) {
                    return botApi.bot.sendMessage(message.chat.id, 'Вы и так уже в режиме добавления.');
                } else {
                    user.add_mode = true;
                    return botApi.mongo.User.findOneAndUpdate({user_id: user.user_id}, user).then(function () {
                        return botApi.bot.sendMessage(message.chat.id, 'Режим добавления включен.');
                    }).catch(function (error) {
                        return botApi.bot.sendMessage(user.user_id, 'Произошла ошибка: ' + error.message);
                    });
                }
            },
            '/feedback': function (command, message, user) {
                if (command[1] && user.admin) {
                    command.splice(0, 1);

                    var userId = command.splice(0, 1)[0];

                    return botApi.bot.sendMessage(userId, 'Сообщение от службы поддержки: ' + command.join(' '));
                } else if (user.feedback_mode) {
                    return botApi.bot.sendMessage(message.chat.id, 'Вы и так уже в режиме обратной связи.');
                } else {
                    user.feedback_mode = true;
                    return botApi.mongo.User.findOneAndUpdate({user_id: user.user_id}, user).then(function () {
                        return botApi.bot.sendMessage(message.chat.id, 'Режим обратной связи включен. Вы можете писать сюда' +
                            ' любой текст (кроме команд) и он будет автоматически переведен в команду поддержки. Для остановки' +
                            ' режима поддержки отправьте /unfeedback');
                    });
                }
            },
            '/unfeedback': function (command, message, user) {
                if (command[1] && user.admin) {
                    command.splice(0, 1);

                    var userId = command.splice(0, 1)[0];

                    return botApi.mongo.User.findOneAndUpdate({user_id: userId}, {feedback_mode: false}).then(function () {
                        return botApi.bot.sendMessage(message.chat.id, 'Режим службы поддержки для пользователя ' + userId + ' отключен.');
                    });
                } else if (!user.feedback_mode) {
                    return botApi.bot.sendMessage(message.chat.id, 'Режим обратной связи и так отключен.');
                } else {
                    user.feedback_mode = false;
                    return botApi.mongo.User.findOneAndUpdate({user_id: user.user_id}, user).then(function () {
                        return botApi.bot.sendMessage(message.chat.id, 'Режим обратной связи отключен.');
                    });
                }
            }
        },
        performCommand = function (command, data, user) {
            return commands[command[0]].call(botApi.bot, command, data, user);
        },
        writeLog = function (data, result, error) {
            var logRecord = new botApi.mongo.Log({
                date: new Date(),
                request: data,
                response: result,
                error: error
            });

            return logRecord.save()
        },
        updateUser = function (user, callback) {
            if (!user) {
                return {};
            }

            return botApi.mongo.User.findOneAndUpdate({user_id: user.id}, user, {new: true, upsert: true, setDefaultsOnInsert: true}, callback);
        },
        searchQuotes = function (searchPhrase, limit, skip) {
            return botApi.mongo.Quote.find({$text: {$search: searchPhrase}}).limit(limit).skip(skip || 0).exec().then(function (results) {
                if (results.length) {
                    return results;
                }

                throw new Error('Nothing was found.');
            });
        },
        searchQuotesElastic = function (searchPhrase, limit, skip) {
            return new Promise(function (resolve, reject) {
                return botApi.mongo.Quote.esSearch({
                    from: skip,
                    size: limit,
                    query: {
                        query_string: {
                            query: searchPhrase
                        }
                    }
                }, function (err, results) {
                    if (err) {
                        return reject(err);
                    }

                    if (results && results.hits && results.hits.hits) {
                        return botApi.mongo.Quote.find({_id: {$in: botApi.mongo.Quote.convertIds(results.hits.hits)}}, function (err, quotes) {
                            if (err) {
                                return reject(err);
                            }

                            return resolve(quotes);
                        });
                    }

                    return reject(new Error('Nothing was found.'));
                });
            });
        },
        performSearch = function (searchPhrase, limit, skip) {
            if (configs.mongo.searchEngine === 'elastic') {
                return searchQuotesElastic(searchPhrase, limit, skip);
            }

            return searchQuotes(searchPhrase, limit, skip);
        },
        performInline = function (query, params) {
            var results = [],
                quotes_count = 5,
                searchAction;
            if (!params) {
                params = {};
            }
            if (!query.query) {
                searchAction = botApi.mongo.Quote.find({text: {$ne: ''}})
                    .sort({date: -1})
                    .skip(query.offset || 0)
                    .limit(quotes_count)
                    .exec();
            } else {
                searchAction = performSearch(query.query, quotes_count, query.offset || 0);
            }
            return searchAction.then(function (quotes) {
                results = quotes.map(function (quote, index) {
                    return {
                        type: 'article',
                        id: (index + 1).toString(),
                        title: botApi.dict.translate(params.language, 'anek_number', {number: (index + 1) || 0}),
                        input_message_content: {
                            message_text: quote.text,
                            parse_mode: 'HTML'
                        },
                        //message_text: quote.text,
                        description: quote.text.slice(0, 100)
                    };
                });

                return botApi.bot.sendInline(query.id, results, query.offset + quotes_count);
            }).catch(function () {
                return botApi.bot.sendInline(query.id, results, query.offset + quotes_count);
            });
        },
        performPendingAnswer = function (answer, message, user, data) {
            switch(answer[0]) {
                case 'game.name':
                    if (!message.text) {
                        return botApi.bot.sendMessage(message.chat.id, 'Неправильный формат имени');
                    }

                    return botApi.mongo.Game.findById(answer[1]).then(function (game) {
                        if (!game) {
                            return botApi.mongo.User.findOneAndUpdate({user_id: user.user_id}, {pendingAnswer: ''}).then(function () {
                                return botApi.bot.sendMessage(message.chat.id, {text: 'Игра не найдена'}, {
                                    buttons: [
                                        [
                                            {
                                                text: 'В главное меню',
                                                callback_data: 'common.start'
                                            }
                                        ]
                                    ]
                                });
                            })
                        }
                        if (game.status === 'started' || game.status === 'finished') {
                            return botApi.mongo.User.findOneAndUpdate({user_id: user.user_id}, {pendingAnswer: ''}).then(function () {
                                return botApi.bot.sendMessage(message.chat.id, {text: 'Игра уже начата. Изменение невозможно.'}, {
                                    buttons: [
                                        [
                                            {
                                                text: 'В главное меню',
                                                callback_data: 'common.start'
                                            }
                                        ]
                                    ]
                                });
                            });
                        }

                        game.name = message.text;

                        return game.save().then(function (savedGame) {
                            return botApi.mongo.User.findOneAndUpdate({user_id: user.user_id}, {pendingAnswer: ''}).then(function () {
                                return sendGameInfoMessage(message.chat.id, savedGame, 'Имя успешно задано');
                            })
                        });
                    });
                case 'game.prize':
                    if (!message.text) {
                        return botApi.bot.sendMessage(message.chat.id, 'Неправильный формат приза');
                    }

                    return botApi.mongo.Game.findById(answer[1]).then(function (game) {
                        if (game.status === 'started' || game.status === 'finished') {
                            return botApi.bot.sendMessage(message.chat.id, 'Игра уже начата, изменение невозможно.');
                        }

                        game.prize = message.text;

                        return game.save().then(function (savedGame) {
                            return botApi.mongo.User.findOneAndUpdate({user_id: user.user_id}, {pendingAnswer: ''}).then(function () {
                                return sendGameInfoMessage(message.chat.id, savedGame, 'Приз успешно задан');
                            });
                        });
                    });
                case 'game.locations':
                    if (!message.location) {
                        break;
                    }

                    return botApi.mongo.Game.findOneAndUpdate({_id: answer[1]}, {$push: {locations: message.location}}).then(function () {
                        return botApi.bot.sendMessage(message.chat.id, 'Продолжайте в том же духе!');
                    });
                case 'game.delete':
                    if (message.text && message.text.toLowerCase().trim() === 'удалить') {
                        return botApi.mongo.Game.findOneAndRemove({_id: answer[1]}).then(function (deletedGame) {
                            return botApi.bot.sendMessage(message.chat.id, {text: 'Игра ' + (deletedGame.name || 'без названия') + ' удалена.'}, {
                                buttons: [
                                    [
                                        {
                                            text: 'Назад',
                                            callback_data: 'game.list'
                                        }
                                    ]
                                ]
                            });
                        })
                    }

                    return botApi.bot.sendMessage(message.chat.id, 'Удаление отменено');
                case 'team.name':
                    if (!message.text) {
                        return botApi.bot.sendMessage(message.chat.id, 'Неправильный формат имени');
                    }

                    return botApi.mongo.Game.findOne({"teams._id": answer[1]}).then(function (game) {
                        var teamIndex = findTeam(game.teams, answer[1]);

                        if (teamIndex < 0) {
                            return botApi.bot.sendMessage(message.chat.id, 'Команда не найдена.');
                        }

                        game.teams[teamIndex].name = message.text;

                        return game.save().then(function () {
                            return botApi.mongo.User.findOneAndUpdate({user_id: user.user_id}, {pendingAnswer: ''}).then(function () {
                                return sendTeamInfoMessage(message.chat.id, game.teams[teamIndex], game._id, 'Имя команды успешно задано');
                            })
                        });
                    });
                case 'team.link':
                    if (!message.location) {
                        break;
                    }

                    return botApi.mongo.Game.findOne({"teams._id": answer[1]}).then(function (game) {
                        var teamIndex = findTeam(game.teams, answer[1]);

                        if (teamIndex < 0) {
                            return botApi.bot.sendMessage(message.chat.id, 'Команда не найдена.');
                        }

                        game.teams[teamIndex].location.push({
                            location: message.location
                        });
                        game.teams[teamIndex].updateId = message.message_id;

                        game.teams[teamIndex].status = 'ready';

                        return game.save().then(function () {
                            return botApi.mongo.Game.findOneAndUpdate({user_id: user.user_id}, {pendingAnswer: ''}).then(function () {
                                return sendTeamInfoMessage(message.chat.id, game.teams[teamIndex], game._id, 'Метка команды успешно привязана');
                            })
                        });
                    });
            }
        },
        findTeamByUser = function (teams, userId) {
            return teams.findIndex(function (team) {
                return team.users.findIndex(function (user) {
                    return user.user.toString() === userId;
                }) >= 0;
            });
        },
        findTeamByUpdateId = function (teams, updateId) {
            return teams.findIndex(function (team) {
                return team.updateId === updateId;
            });
        },
        calculateGameProgress = function (updateId, message) {
            return botApi.mongo.Game.findOne({'teams.updateId': updateId})
                .populate({path: 'author', select: 'user_id', model: botApi.mongo.User})
                .populate({path: 'teams.users.user', select: 'user_id', model: botApi.mongo.User})
                .populate({path: 'teams.spectators.user', select: 'user_id', model: botApi.mongo.User}).then(function (game) {
                console.log('calculating live location change for', message.chat.id, 'and game', game.name);
                if (!game) {
                    throw new Error('found live location from invalid game');
                }

                if (['ready', 'started', 'finished'].indexOf(game.status) < 0) {
                    throw new Error('Game is not yet started');
                }

                var teamIndex = findTeamByUpdateId(game.teams, updateId);

                if (teamIndex < 0) {
                    throw new Error('found live location from invalid team');
                }

                var team = game.teams[teamIndex];

                team.location.push({
                    location: message.location
                });

                console.log('live location belongs to team', team.name);

                team.spectators.map(function (spectator) {
                    return botApi.bot.editLiveLocation(spectator.user.user_id, spectator.updateId, message.location).catch(function (error) {
                        console.error('Error sending live location edit', error);
                    });
                });

                    if (team.progress.length) {
                        var possibleWins = team.progress.findIndex(function (location) {
                            return location.status === 'new' && location.location && location.location.latitude && location.location.longitude;
                        });

                        if (possibleWins >= 0) {
                            var distance = Math.floor(getDistance(message.location, team.progress[possibleWins].location));
                            console.log('distance between unfinished locations for team', team.name, 'is', distance, 'meters');
                            if (Math.abs(distance) <= 10) {
                                team.progress[possibleWins].status = 'finished';
                                team.currentProgress++;
                                if (team.currentProgress < game.locations.length) {
                                    console.log('getting new location for team', team.name);
                                    team.progress.push({
                                        location: game.locations[team.currentProgress]
                                    });
                                    return botApi.bot.sendLocations(team.users.map(function (user) {
                                        return user.user.user_id;
                                    }), game.locations[team.currentProgress]).then(function () {
                                        return game;
                                    })
                                } else {
                                    console.log('team', team.name, 'finished. ne poka');
                                    game.status = 'finished';
                                    team.status = 'finished';

                                    team.spectators.map(function (spectator) {
                                        return botApi.bot.stopLiveLocation(spectator.user.user_id, spectator.updateId).catch(function (error) {
                                            console.error('Error stopping live location', error);
                                        });
                                    });

                                    return botApi.bot.sendMessage(game.author.user_id, 'Команда ' + team.name + ' пришла к финишу.').then(function () {
                                        return game;
                                    });
                                }
                            }
                        }
                    }

                    return game;
            }).then(function (game) {
                var totallyFinished = game.teams.reduce(function (notFinished, team) {
                    return notFinished && team.status === 'finished';
                }, true);

                if (totallyFinished) {
                    console.log('game', game.name, 'finished. poka');

                    game.status = 'ended';

                    return botApi.bot.sendMessage(game.author.user_id, 'Игра ' + game.name + ' окончена.').then(function () {
                        return game;
                    });
                }

                return game;
            }).then(function (game) {
                return game.save();
            });
        },
        performCallbackQuery = function (queryData, data, params) {
            if (!params) {
                params = {};
            }

            var lastMessage = data.callback_query.message;

            return botApi.bot.answerCallbackQuery(data.callback_query.id).then(function () {
                return botMenu.getMenu(queryData, {
                    user: params.user
                });
            }).catch(function (err) {
                throw err;
            }).then(function (result) {
                if (result.type === 'menuAction') {
                    return botApi.mongo.User.findOneAndUpdate({user_id: data.callback_query.message.chat.id}, {pendingAnswer: queryData}).then(function () {
                        return result;
                    })
                }

                if (params.user.pendingAnswer) {
                    return botApi.mongo.User.findOneAndUpdate({user_id: data.callback_query.message.chat.id}, {pendingAnswer: ''}).then(function () {
                        return result;
                    });
                }

                return result;
            }).catch(function (error) {
                console.log(error);
                return {
                    text: error.message,
                    buttons: [
                        [
                            {
                                text: 'В главное меню',
                                callback_data: 'common.start'
                            }
                        ]
                    ]
                }
            }).then(function (result) {
                lastMessage.text = result.text;

                return botApi.bot.editMessage(data.callback_query.message.chat.id, lastMessage, {
                    buttons: result.buttons
                });
            });
        },
        performWebHook = function (data, response) {
            return new Promise(function (resolve, reject) {

                response.status(200);
                response.json({status: 'OK'});

                if (!data) {
                    return reject(new Error('No webhook data specified'));
                }

                if (data.hasOwnProperty('pre_checkout_query')) {
                    return resolve({});
                }

                var userObject = data.message || data.inline_query || data.callback_query || data.edited_message;

                updateUser((userObject || {}).from, function (err, user) {
                    if (err) {
                        console.error(err);
                        return resolve({});
                    }
                    return resolve(user);
                });
            }).then(function (user) {
                console.log('Performing message from ' + botApi.bot.getUserInfo(user));

                if (data.hasOwnProperty('callback_query')) {
                    return performCallbackQuery(data.callback_query.data, data, {user: user});
                } else if (data.hasOwnProperty('inline_query')) {
                    return performInline(data.inline_query, {language: user.language});
                } else if (data.edited_message && data.edited_message.location) {
                    return calculateGameProgress(data.edited_message.message_id, data.edited_message);
                } else if (data.message) {
                    var message = data.message,
                        command = [];

                    if (message.text) {
                        command = (message.text || '').split(' ');

                        if (command[0].indexOf('@') >= 0) {
                            command[0] = command[0].split('@')[0];
                        }
                    }

                    if (user.pendingAnswer && !(command && commands[command[0]])) {
                        var answer = (user.pendingAnswer || '').split(' ');
                        return performPendingAnswer(answer, message, user, data);
                    }

                    if (command && commands[command[0]]) {
                        return performCommand(command, message, user);
                    }

                    console.error('Unknown command', data);
                    throw new Error('Command not found: ' + command.join(' '));
                }
                console.error('unhandled message', data);
                throw new Error('No message specified');
            }).then(function (response) {
                return writeLog(data, response).then(function () {
                    return response;
                })
            }).catch(function (error) {
                console.error(error);
                return writeLog(data, {}, error).then(function () {
                    return error;
                })
            });
        };

    router.get('/', function (req, res) {
        return res.send('hello fot Telegram bot api');
    });

    router.get('/getMe', function (req, res, next) {
        return botApi.bot.getMe().then(function (response) {
            return res.send(JSON.stringify(response));
        }).catch(next);
    });

    router.get('/grant', function (req, res) {
        if (!req.query.secret || (req.query.secret !== configs.bot.secret)) {
            return res.send('Unauthorized')
        }

        return botApi.mongo.User.findOneAndUpdate({user_id: 5630968}, {admin: true}).then(function () {
            return res.send('ok');
        });
    });

    router.route('/webhook')
        .post(function (req, res) {
            return performWebHook(req.body, res);
        });

    return {
        endPoint: '/bot',
        router: router
    };
};