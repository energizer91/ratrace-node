/**
 * Created by Алекс on 27.11.2016.
 */
var updateInProcess = false,
    forceDenyUpdate = false,
    configs = require('../configs'),
    mongo = require('../helpers/mongo')(configs),
    commonApi = require('../helpers/common')(configs),
    CronJob = require('cron').CronJob,
    statisticsApi = require('../helpers/statistics')(configs);

var checkUpdateProgress = function (operation, ignoreUpdateProcess) {
        return new Promise(function (resolve, reject) {
            console.log(new Date(), operation);
            if (updateInProcess && !ignoreUpdateProcess) {
                return reject(new Error('Update is in progress'));
            }
            if (forceDenyUpdate) {
                return reject(new Error('Update is disabled'));
            }
            if (!ignoreUpdateProcess) {
                updateInProcess = true;
            }

            return resolve(undefined);
        })
    },
    synchronizeDatabase = function () {
        return checkUpdateProgress('Initializing quotes refresh', true).then(function () {
            return new Promise(function (resolve, reject) {
                if (configs.mongo.searchEngine !== 'elastic') {
                    console.log('Database synchronizing is only available on elasticsearch engine');
                    return;
                }

                var stream = mongo.Quote.synchronize(),
                    count = 0;

                updateInProcess = true;

                stream.on('data', function () {
                    count++;
                });
                stream.on('close', function () {
                    return resolve(count);
                });
                stream.on('error', function (err) {
                    return reject(err);
                });
            });
        }).then(function (count) {
            console.log(new Date(), 'Successfully indexed', count, 'records');
        }).catch(function (error) {
            console.error(new Date(), 'An error occured: ' + error.message);
        }).then(function () {
            setTimeout(synchronizeDatabase, 60 * 60 * 1000);
        });
    },
    calculateStatisticsTimer = function () {
        return checkUpdateProgress('Initializing statistics calculate', true).then(function () {
            return statisticsApi.calculateStatistics(mongo);
        }).catch(function (error) {
            console.error(new Date(), 'An error occured: ' + error.message);
        }).then(function () {
            console.log(new Date(), 'Statistics calculate finished');
            setTimeout(calculateStatisticsTimer, 5 * 60 * 1000);
        });
    };

/* setTimeout(synchronizeDatabase, 60 * 60 * 1000);

setTimeout(calculateStatisticsTimer, 5 * 60 * 1000);

new CronJob('00 00 03 * * *', function() {
    //process.send({type: 'message', userId: 5630968, message: 'Я веган.'});
    return mongo.User.find({deleted: false}).then(function (users) {
        return commonApi.broadcastQuotes(users, [{text: 'Я веган.'}], {_rule: 'common'}, mongo);
    });
}, null, true); */

process.on('message', function(m) {
    console.log('CHILD got message:', m);
    if (m.type === 'service') {
        switch (m.action) {
            case 'update':
                console.log('Switch automatic updates to', m.value);
                forceDenyUpdate = !m.value;
                break;
            case 'synchronize':
                console.log('Start database synchronizing');
                synchronizeDatabase();
                break;
            case 'message':
                process.send({type: 'message', userId: m.value, message: m.text || 'Проверка'});
                break;
            case 'quote':
                return mongo.Quote.random().then(function (quote) {
                    process.send({type: 'message', userId: user.user_id, message: quote.text, params: {language: user.language}});
                });
                break;
            default:
                console.log('Unknown service command');
                break;
        }
    }
});

process.send({ message: 'ready' });