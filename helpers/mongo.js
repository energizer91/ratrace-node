/**
 * Created by xgmv84 on 11/26/2016.
 */

module.exports = function (configs) {
    var mongoose = require('mongoose'),
        mongoosastic = require('mongoosastic'),
        config = configs.mongo,
        db = mongoose.connection;

    mongoose.Promise = Promise;

    db.on('error', console.error);

    db.once('open', function () {
        console.log('MongoDB connection successful');
    });

    var userSchema = mongoose.Schema({
            username: String,
            first_name: String,
            last_name: String,
            user_id: Number,
            deleted: {type: Boolean, default: false},
            add_mode: {type: Boolean, default: false},
            feedback_mode: {type: Boolean, default: false},
            admin: {type: Boolean, default: false},
            editor: {type: Boolean, default: false},
            date: {type: Date, default: Date.now},
            pendingAnswer: String,
            menuMessageId: Number
        }),
        locationSchema = mongoose.Schema({
            longitude: Number,
            latitude: Number
        }),
        teamSchema = mongoose.Schema({
            name: String,
            users: [{
                user: {type: mongoose.Schema.Types.ObjectId, ref: userSchema},
                role: {type: String, default: 'spectator', enum: ['owner', 'navigator', 'spectator']}
            }],
            updateId: Number,
            location: [{
                location: locationSchema,
                timestamp: {type: Date, default: Date.now}
            }],
            progress: [{
                location: locationSchema,
                status: {type: String, default: 'new', enum: ['new', 'finished', 'failed']},
                timestamp: {type: Date, default: Date.now}
            }],
            spectators: [{
                user: {type: mongoose.Schema.Types.ObjectId, ref: userSchema},
                updateId: Number
            }],
            status: {type: String, default: 'new', enum: ['new', 'ready', 'finished']},
            currentProgress: {type: Number, default: 0}
        }),
        gameSchema = mongoose.Schema({
            author: {type: mongoose.Schema.Types.ObjectId, ref: userSchema},
            name: {type: String, default: 'Игра без названия'},
            locations: [locationSchema],
            teams: [teamSchema],
            spectators: [{type: mongoose.Schema.Types.ObjectId, ref: userSchema}],
            timestamp: {type: Date, default: Date.now},
            timeLimit: Date,
            prize: String,
            status: {type: String, default: 'new', enum: ['new', 'ready', 'started', 'finished', 'ended']}
        }),
        logSchema = mongoose.Schema({
            date: {
                type: Date,
                expires: 60 * 60 * 24 * 7,
                default: Date.now
            },
            request: Object,
            response: Object,
            error: Object
        }),
        statisticsSchema = mongoose.Schema({
            users: {
                count: Number,
                new: Number,
                deleted: Number
            },
            messages: {
                received: Number,
                popularCommand: String
            },
            date: {
                type: Date,
                expires: 60 * 60 * 24 * 365,
                default: Date.now
            }
        });

    gameSchema.statics.convertId = function (id) {
        return mongoose.Types.ObjectId(id);
    };

    // quoteSchema.statics.convertIds = function (ids) {
    //     if (Array.isArray(ids)) {
    //         return ids.map(function (id) {
    //             return mongoose.Types.ObjectId(id._id);
    //         })
    //     }
    //
    //     return [];
    // };

    mongoose.connect('mongodb://' + config.server + '/' + config.database);

    // if (config.searchEngine === 'elastic') {
    //     quoteSchema.plugin(mongoosastic, {
    //         hosts: config.elasticHosts
    //     });
    // } else if (config.searchEngine === 'native') {
    //     quoteSchema.index({text: "text"}, {weights: {content: 10, keywords: 5}, name: "text_text", default_language: "russian"});
    // }


    return {
        Game: mongoose.model('Game', gameSchema),
        User: mongoose.model('User', userSchema),
        Log: mongoose.model('Log', logSchema),
        Statistic: mongoose.model('Statistic', statisticsSchema)
    };
};