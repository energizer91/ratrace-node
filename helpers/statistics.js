/**
 * Created by Александр on 19.06.2017.
 */

module.exports = function (configs) {
    return {
        calculateUserStatistics: function (db, from, to) {
            var result = {
                count: 0,
                new: 0,
                deleted: 0
            },
                fromDate = new Date(from || 0),
                toDate = new Date(to || Date.now());
            return db.User.count({}).then(count => {
                result.count = count;

                //console.log('users count', count);

                //return db.Log.find({'request.message.text': '/subscribe', date: {$gte: fromDate, $lte: toDate}}).count();
                return db.User.find({deleted: true}).count();
            }).then(count => {
                result.deleted = count;

                return db.User.find({date: {$gte: fromDate, $lte: toDate}}).count();
            }).then(count => {
                result.new = count;

                //console.log('new count', count);

                return result;
            });
        },
        calculateMessagesStatistics: function (db, from, to) {
            var result = {
                    received: 0
                },
                fromDate = new Date(from || 0),
                toDate = new Date(to || Date.now());
            return db.Log.count({date: {$gte: fromDate, $lte: toDate}}).then(count => {
                result.received = count;

                return result;
            });
        },
        calculateStatistics: function (db, from, to) {
            var result = {
                users: {},
                messages: {},
                date: new Date()
            };

            return db.Statistic.find({}).sort({date: -1}).limit(1).exec().then(statistics => {
                if (!(statistics && statistics.length)) {
                    from = 0;
                } else {
                    from = statistics[0].date;
                }

                if (!to) {
                    to = new Date();
                }

                return this.calculateUserStatistics(db, from, to);
            }).then(users => {
                result.users = users;

                return this.calculateMessagesStatistics(db, from, to);
            }).then(messages => {
                result.messages = messages;

                return new db.Statistic(result).save().then(() => {
                    return result;
                })
            });
        },
        getOverallStatistics: function (db, from, to) {
            return db.Statistic.find({date: {$gte: from, $lte: to}})
                .then(result => result.reduce((p, c) => {
                    p.users.count = c.users.count;
                    p.users.new += c.users.new;
                    p.users.deleted = c.users.deleted;

                    p.messages.received += c.messages.received;

                    return p;
                }, {
                    users: {
                        count: 0,
                        new: 0,
                        deleted: 0
                    },
                    messages: {
                        received: 0
                    }
                }
            ));
        }
    };
};