function Menu(config, methods) {
    this.methods = methods || {};

    this.config = config || {};
}

Menu.prototype.getMenuItem = function (path, substructure) {
    if (!path || !path.length) {
        return substructure || undefined;
    }

    var menuNode = (substructure || this.config)[path[0]];

    if (menuNode && menuNode.type && typeof menuNode.type === 'string') {
        return menuNode;
    }

    return this.getMenuItem(path.slice(1), menuNode);
};

Menu.prototype.getMenu = function (path, helpers) {
    if (!path) {
        throw new Error('Menu path is empty');
    }
    var paramString = path.split(' '),
        menuItems = paramString[0].split('.'),
        params = paramString.slice(1);

    var menuItem = this.getMenuItem(menuItems);

    if (!menuItem) {
        throw new Error('Unknown menu ' + path);
    }

    if (typeof menuItem === 'function') {
        return menuItem.call(null, params, this.methods, helpers, {});
    }

    return menuItem;
};

module.exports = Menu;