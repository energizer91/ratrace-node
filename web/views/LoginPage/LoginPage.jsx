import React from 'react';
import LoginForm from '../../components/LoginForm/LoginForm';

const HomePage = () => (
    <LoginForm />
);

export default HomePage;